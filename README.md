### Ambiente de Desenvolvimento
- conda create -n djangoenv --no-default-packages
- activate djangoenv
- pip install -r requirements.txt (na pasta do projeto)

### Arrumar banco de dados
- Apagar todos os migrates
- Apagar todos os pycache
- Apagar db.sqlite3
- python manage.py makemigrations
- python manage.py migrate
- python manage.py createsuperuser

### GITLAB USP
- git clone https://gitlab.uspdigital.usp.br/tomasolenscki/USPesos.git

### UPAR NO GITHUB
- git remote add github https://github.com/tomasolenscki/USPesos (primeira vez)
- git push --mirror github
- git clone https://github.com/tomasolenscki/USPesos.git

### PythonAnywhere
- Não mudar nada pelo editor do próprio site
- git pull
- python manage.py collectstatic
- link: http://uspesos.pythonanywhere.com/

### Usuários
- admin/adm123456

#### Senha comum: senhasenha
- aluno 1 - Tsuzuki
- aluno 2 - Ednaldo

- professor 1 - Balestrin
- professor 2 - Cariani

